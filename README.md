# SimpleGFX Gamepad

A Gamepad library for SimpleGFX.
This adds gamepad support the eventSystem that is part of SimpleGFX.
Because of this, the code can easily be combined with other input methods

The code works cross-platform and is based on (OIS)[https://github.com/wgois/OIS].
