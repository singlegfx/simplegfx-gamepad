option(
    'build_tests',
    type: 'feature',
    value: 'auto',
    description: 'set to false if you do not want to compile the tests. Tests won\'t be built if this is a subproject unless explicitly enabled.'
)
